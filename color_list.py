import RPi.GPIO as GPIO

bluPin = 40
redPin = 37
grnPin = 38

GPIO.setmode(GPIO.BOARD)
GPIO.setup(bluPin, GPIO.OUT)
GPIO.setup(redPin, GPIO.OUT)
GPIO.setup(grnPin, GPIO.OUT)

def makepurple():
    GPIO.output(bluPin, GPIO.HIGH)
    GPIO.output(redPin, GPIO.HIGH)
    GPIO.output(grnPin, GPIO.LOW)
    #print('Turning purple')

def makeyellow():
    GPIO.output(bluPin, GPIO.LOW)
    GPIO.output(redPin, GPIO.HIGH)
    GPIO.output(grnPin, GPIO.HIGH)
    #print('Turning yellow')

def maketeal():
    GPIO.output(bluPin, GPIO.HIGH)
    GPIO.output(redPin, GPIO.LOW)
    GPIO.output(grnPin, GPIO.HIGH)
    #print('Turning teal(?)')

def makewhite():
    GPIO.output(bluPin, GPIO.HIGH)
    GPIO.output(redPin, GPIO.HIGH)
    GPIO.output(grnPin, GPIO.HIGH)
    #print('Turning white')

def makeblue():
    GPIO.output(bluPin, GPIO.HIGH)
    GPIO.output(redPin, GPIO.LOW)
    GPIO.output(grnPin, GPIO.LOW)
    #print('Turning blue')

def makered():
    GPIO.output(bluPin, GPIO.LOW)
    GPIO.output(redPin, GPIO.HIGH)
    GPIO.output(grnPin, GPIO.LOW)
    #print('Turning red')

def makegreen():
    GPIO.output(bluPin, GPIO.LOW)
    GPIO.output(redPin, GPIO.LOW)
    GPIO.output(grnPin, GPIO.HIGH)
    #print('Turning green')

def makeoff():
    GPIO.output(bluPin, GPIO.LOW)
    GPIO.output(redPin, GPIO.LOW)
    GPIO.output(grnPin, GPIO.LOW)
    #print('Turning off')

def switchto(color):
    if color == 'RED':
        makered()
    elif color == 'GRN':
        makegreen()
    elif color == 'BLU':
        makeblue()
    elif color == 'TEL':
        maketeal()
    elif color == 'YLO':
        makeyellow()
    elif color == 'PRP':
        makepurple()
    elif color == 'WHT':
        makewhite()
    elif color == 'OFF':
        makeoff()
    else:
        raise Exception('unrecognised switchto argument ' + color)
