# pilut

Power-Indicating Light UniT: A linux service that controls a GPIO-connected led to indicate power, network and cpu status on a Raspberry Pi

### DISCLAIMER
This software and its documentation are amateur-hour stuff and, as such, are provided without warranties of absolutely any kind.

### THE PROBLEM
I have a Raspberry Pi B+ that I run via SSH.
Unfortunately, the board LEDs are not well-suited for this particular use case; the power LED will be lit as long as power is connected, even if the Pi is in a shutdown state.
Consequently, if I attempt to log in for the first time in a while and can't get in, it isn't immediately obvious whether the Pi is disconnected from the network, shutdown etc.
I want the ability to diagnose this and other issues at a glance, without leaving my posting station.

### MY SOLUTION
`pilut.py` runs as a systemd service to drive an RGB LED through the Pi's GPIO pins. The LED lights during boot-up and changes colour to indicate:

1. whether or not a specified IP address could be successfully pinged
2. whether a ping is in progress
3. whether or not the cpu usage in the previous interval was over a specified threshold

### REQUIREMENTS AND DEPENDENCIES
+ Python 2.x
+ psutil, pyping, RPi.GPIO modules for Python
+ RGB LED
+ Raspberry Pi running Linux
+ resistors, leads etc suitable for connecting your model of LED to the Pi's GPIO

### INSTALLATION
0. Connect an RGB LED to your RPi GPIO pins using leads and resistors. Make a note of which pins are used for the R, G and B pins of the LED.
1. Install the psutil, pyping and RPi.GPIO modules (if you have pip, try `pip install <module>`).
2. Make a directory under your `/home/your_username/` directory and `cd` to it. Clone the pilut repo to this location using `git clone git@bitbucket.org:softerplastics/pilut.git` and then move (`mv source_file destination`) each file to an appropriate location. Make a note of where each file goes because you'll need it later.
    + `pilut.py` should go to `/bin` or wherever your system binaries go. Let's call this Location A. `cd` to this location then `sudo chmod u+x pilut.py` to set permissions
    + `color_list.py` and `pilut_settings.txt` can go wherever you keep python modules you've written, or in a directory under home. This is Location B. Make a new blank file in this location called `__init__.py` if one doesn't already exist there.
    + `pilut.service` should go where systemd's .service files are kept, which is usually the `/etc/systemd/system` directory. This is Location C.
    + Find the path to your python binary (typically `/bin/python`).  This is Location D. You don't need to move any files here.
3. In a text editor, go through `pilut.py` and make sure the paths mentioned at lines 3, 4 and 6 (or nearby) match the path to your Location B directory. Look for the comments. Make sure that the path mentioned at line 1 points to Location D and has `#!` in front of it.
4. In a text editor, go through `pilut.service` and find line 11 (or nearby) which begins `ExecStart=`. Make sure the rest of the line matches the path to your Location A directory.
5. Open `pilut_settings.txt` in a text editor and set the options for the following variables.
    + `address`: the IP address to ping. Suggested value: router's address.
    + `load_hi_if_over`: percentage at which the CPU is deemed to be under high load, as a number between 0 and 100. Suggested value: 70
    + `network_timer`: The number of seconds between ping attempts. Suggested value: 15
    + `tick`: The number of seconds between CPU load calculations. Higher values are less useful unless the Pi runs flat out most of the time. This value should be smaller than `network_timer`. Suggested value: 1
6. In a text editor, go through lines 3, 4 and 5 of `color_list.py` and make sure that the pin numbers correspond to the pins you are using for pilut's LED. Note that by default the `BOARD` numbering scheme (not the `BCM` numbering scheme) is used.
7. You should now be able to enable pilut to start on boot: `sudo systemctl enable pilut`

### WHAT DO THE COLOURS MEAN?
By default, the colours correspond to the following states:

| | |CPU LOAD|LAST PING SUCCESSFUL|LAST PING UNSUCCESSFUL|CURRENTLY PINGING| | |
|---|---|---|---|---|---|---|---|
| | |**HIGH**|teal (TEL)|yellow (YLO)|white (WHT)| | |
| | |**LOW**|blue (BLU)|red (RED)|white (WHT)| | |

These functions require the use of 5 of the 7 colours the LED can produce.

Editing `pilut_settings.txt` allows you to change which LED colour indicates a given status. Your options are `RED` (red), `GRN` (green), `BLU` (blue), `TEL` (teal/aqua), `YLO` (yellow), `PRP` (purple), `WHT` (white) or `OFF` (not lit). The default colours have been chosen to be suitable for users with red-green colour-blindness.

  + `status_hi_load_lo_net`: colour when CPU load is over threshold and the last ping was unsuccessful. default: YLO
  + `status_lo_load_lo_net`: colour when CPU load is under threshold and the last ping was unsuccessful. default: RED
  + `status_hi_load_hi_net`: colour when CPU load is over threshold and the last ping was successful. default: TEL
  + `status_lo_load_hi_net`: colour when CPU load is over threshold and the last ping was successful. default: BLU
  + `status_ping`: colour shown between sending and receiving a network ping.  default: WHT

### CONTACT
softerplastics@mail.com

### THANKS TO
+ Contributors to the modules on which pilut relies
+ Community members on the Raspberry Pie discord server
+ JIMBLOM and MTAYLOR @ sparkfun for this tutorial https://learn.sparkfun.com/tutorials/raspberry-gpio/introduction
+ Ben Morel for this tutorial https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6
+ Approximate Engineering for this tutorial https://approximateengineering.org/2017/04/running-python-as-a-linux-service/
+ @wizardfortress for variety of useful advice and priceless colour vision

### GR33TZ
`W0RM5C#47`