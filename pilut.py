#!/bin/python
import sys
sys.path.append('/home/pi') #change to the directory ABOVE your Location B
from python_files import color_list as led #change python_files to the name (not the full path) of your Location B directory
opt = {}
with open('/home/pi/python_files/pilut_settings.txt', 'r') as f: #change the first string to the path to pilut_settings.txt in your Location B
    for line in f:
        key, value = line.split("=")
        opt[key.strip()] = value.strip()

import os
from signal import signal, SIGINT, SIGTERM
from time import sleep
import pyping
import psutil

def get_shutdown_handler(message=None):
    """Return a shutdown handler function with the desired message."""
    def handler(signum, frame):
        """Switch off and cleanup the GPIO features, and print an exit message."""
        led.makeoff()
        led.GPIO.cleanup()
        print(message)
        exit(0)
    return handler

def getLoad():
    """Return True if cpu is over load threshold or False if not."""
    percent = psutil.cpu_percent()
    #print(percent)
    if percent > opt['load_hi_if_over']:
        return True
    return False

def getNet():
    """Return True if a ping to the chosen IP address is successful or False if it fails or raises an Exception."""
    try:
        led.switchto(opt['status_ping'])
        r = pyping.ping(opt['address'], 1000, 1, 55, None, True)
        if r.ret_code == 0:
            return True
        else:
            return False
    except Exception:
        r = 0
        return False
    finally:
        del r

def updateLights(isLoad, isNet):
    """Change the LED to a new colour according on the isLoad and isNet booleans."""
    if isLoad:
        if isNet:
            new_col = opt['status_hi_load_hi_net']
        else:
            new_col = opt['status_hi_load_lo_net']
    else:
        if isNet:
            new_col = opt['status_lo_load_hi_net']
        else:
            new_col = opt['status_lo_load_lo_net']
    led.switchto(new_col)

signal(SIGINT, get_shutdown_handler('SIGINT received by pilut'))
signal(SIGTERM, get_shutdown_handler('SIGTERM received by pilut'))

timer_reset = float(opt['network_timer'])
timer = timer_reset
load = False
network = False
tick_len = float(opt['tick'])

while True:
    timer -= tick_len
    load = getLoad()
    if timer <= 0:
        network = getNet()
        timer = timer_reset
    updateLights(load, network)
    sleep(tick_len)
